﻿using System;

/*
 * Author : Mohan Shanmugarajah
 * Created On : 28-Oct-2018
 * Functional Assumptions : 
 * 1. Share price list order assumed to be in ascending order
 * 2. GAIN or LOSS is calculated based on the previous day performance, as such calculation starts from the second day
 * Technical Assumptions :
 * 1. No test cases added to test varying inputs, edge cases and out of context values
 * 2. Both classes are added wihtin the same namespace for simplicity
 * 3. Tech Statck : .Net, C# for simplicity
 * Steps to run :
 * 1. Open the solution in Visual Studio or Any .Net compatible IDE
 * 2. Hit Run
 */
namespace ANZ.Assessment
{
    /*
     * Ticker which calls the tend analysis for reporting
     */
    public class ShareMarketConsole
    {
        private const string TOTAL_GAIN_LABEL = "Total gain for the period .... ";
        private const string TOTAL_LOSS_LABEL = "Total loss for the period ....";
        private static double[] sharePriceList = new double[] { 78.41, 85.18, 91.09, 90.57, 91.02, 103.61, 105.88, 103.77, 110.13, 108.89, 105.09 };


        public static void Main(string[] args)
        {
            Console.WriteLine(TOTAL_GAIN_LABEL + ShareMarketPeformanceAnalysis.CalculateTotal(sharePriceList, ShareMarketPeformanceAnalysis.ShareMovementType.GAIN));
            Console.WriteLine(TOTAL_LOSS_LABEL + ShareMarketPeformanceAnalysis.CalculateTotal(sharePriceList, ShareMarketPeformanceAnalysis.ShareMovementType.LOSS));
            Console.ReadLine();
        }
    }

    /* 
     * Class which computes the market movement based on the given historical price variation 
     */
    public class ShareMarketPeformanceAnalysis
    {
        public enum ShareMovementType
        {
            GAIN,
            LOSS
        }

        public static double CalculateTotal(double[] sharePriceList, ShareMovementType operation) {
            double total = 0;
            try
            {
                for (int dayIndex = 0; dayIndex <= sharePriceList.Length - 1; dayIndex++)
                {
                    if (dayIndex > 0)
                    {
                        if (operation == ShareMovementType.GAIN)
                        {
                            if (sharePriceList[dayIndex] > sharePriceList[dayIndex - 1])
                            {
                                total = AddDifference(total, sharePriceList, dayIndex);
                            }
                        }


                        if (operation == ShareMovementType.LOSS)
                        {
                            if (sharePriceList[dayIndex] < sharePriceList[dayIndex - 1])
                            {
                                total = AddDifference(total, sharePriceList, dayIndex);
                            }
                        }
                    }
                }

            } catch (Exception ex)
            {
                throw new Exception("CalculateTotal method call failed", ex.InnerException);
            }

            return total;
        }

        private static double AddDifference(double total, double[] sharePriceList, int dayIndex)
        {
            try {
                total = total + (sharePriceList[dayIndex] - sharePriceList[dayIndex - 1]);

            }catch (Exception ex)
            {
                throw new Exception("AddDifference method call failed", ex.InnerException);
            }

            return total;
        }

       
    }
}
